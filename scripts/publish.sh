#!/bin/sh
set -eu #-o pipefail

cd "$(dirname "$(readlink -f "${0}")")/.."
export PATH="$(pwd)/node_modules/web-ext/bin:${PATH}"

# Read API secrets from file
exec 3<"web-ext-api-secret.txt"
IFS='' read -r API_KEY    <&3
IFS='' read -r API_SECRET <&3 || test $? -lt 128
exec 3<&-

# Push GIT repository to remote
git push
git push --tags

# Launch the `web-ext` tool with the read data
EXIT_CODE="$({
	if ! web-ext sign --api-key="${API_KEY}" --api-secret="${API_SECRET}" --channel="listed" "$@";
	then
		echo "__exit_code__:$?"
	else
		echo "__exit_code__:0"
	fi
} | {
	exit_code=1
	success_matched=false
	IFS=""
	while read -r line;
	do
		# Do not record/display lines after our success override line was found
		if ${success_matched};
		then
			continue
		fi

		# Parse the special exit code lines and store the received value
		if [ "${line##__exit_code__:}" != "${line}" ];
		then
			exit_code="${line##__exit_code__:}"
			continue
		fi

		# Simply echo all lines to the screen through stderr
		printf "%s\n" "${line}" >&2

		# Detect success override line (after echoing it) and pretend the command
		# did indeed report success
		case "${line}" in *"passed validation"*"not"*"automatically signed"*)
			success_matched=true
			exit_code=0
			echo SUCCESS >&2
		;; esac
	done

	# Echo the last received exit code to stdout that it will be captured by the parent variable
	echo "${exit_code}" >&1
})" 2>&1
test ${EXIT_CODE} -eq 0 || exit ${EXIT_CODE}
