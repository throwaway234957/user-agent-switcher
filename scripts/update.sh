#!/bin/sh
cd "$(dirname "$(readlink -f "${0}")")"

./user-agent-update.py
if [ $(git diff ../assets/user-agents.txt | wc -l) -gt 0 ];
then
	git commit -m "Automatic User-Agent list update" ../assets/user-agents.txt
fi