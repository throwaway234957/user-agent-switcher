/*
 * User Agent Switcher
 * Copyright © 2017 – 2018  Alexander Schlarb
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Override the properties of the page's global `navigator` object
 * (as well as the object exposed through each IFrame) with the
 * properties from the given data-set object
 * 
 * (This function will be called from the dynamically generated content-script
 * JavaScript snippets.)
 * 
 * @param {utils.uaparser.DataSet} navigatorDataSet
 */
// @ts-nocheck
// eslint-disable-next-line no-unused-vars
function overrideNavigatorData(navigatorDataSet) {
	/** @type {(navigator: Navigator) => void} */
	let applyUserAgentOverride = ((navigator) => {
		let propertyNames = [
			"userAgent",
			"platform",
			"appVersion",
			"cpuClass",
			"oscpu",
			"product",
			"productSub",
			"vendor",
			"vendorSub",
		];
		for(let propertyName of propertyNames) {
			Object.defineProperty(navigator.wrappedJSObject, propertyName, {
				enumerable: navigatorDataSet.hasOwnProperty(propertyName),
				value:      navigatorDataSet.hasOwnProperty(propertyName)
					? navigatorDataSet[propertyName]
					: undefined,
			});
		}
	});
	
	// Override main navigator properties
	applyUserAgentOverride(window.navigator);
	
	// Prevent leakage through properties of trusted iframes
	let observer = new MutationObserver((mutations) => {
		for(let mutation of mutations) {
			for(let node of mutation.addedNodes) {
				if(typeof(node.contentWindow)           !== "undefined"
				&& typeof(node.contentWindow.navigator) !== "undefined") {
					applyUserAgentOverride(node.contentWindow.navigator);
				}
			}
		}
	});
	observer.observe(document.documentElement, {
		childList: true,
		subtree: true
	});
}